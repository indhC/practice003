// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Practice003GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PRACTICE003_API APractice003GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
